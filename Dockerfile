FROM centos:latest

# Install Essentials
RUN yum -y update
RUN yum -y upgrade
RUN yum -y install epel-release

# Install Python, Upgrade Pip
RUN yum -y install python-dev \
                   python-setuptools \
                   python-pip
RUN pip install --upgrade pip


EXPOSE 8080
CMD ["bash"]
